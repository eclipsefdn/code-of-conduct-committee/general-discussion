# README

## Conduct Committee

The Eclipse Foundation is committed to fostering a welcoming, diverse, and inclusive community where all members can collaborate and contribute in a respectful and safe environment. To further this commitment, we are pleased to announce the formation of a Conduct Committee, which will play a crucial role in upholding the core values outlined in the Eclipse Foundation Code of Conduct.

The Conduct Committee is being established to ensure that our community adheres to the highest standards of behavior and maintains a culture of respect, inclusivity, and professionalism, which are fundamental to our core values. This committee will be responsible for addressing issues related to violations of the Eclipse Foundation Code of Conduct and will help maintain a positive and welcoming environment for all contributors.

The Eclipse Conduct Committee charter is availble [here](https://www.eclipse.org/org/documents/eclipse-foundation-conduct-committee-charter.pdf).

## Quarterly reports
For the past couple of years, the Eclipse Foundation has maintained an incident-free record, and the establishment of this committee is solely about good governance, devoid of any specific issues. 

#### The Conduct Committee received **[1 incident report(s](https://gitlab.eclipse.org/eclipsefdn/code-of-conduct-committee/general-discussion/-/issues/1)) in 2024-Q4**
#### The Conduct Committee received **0 incident report(s) in 2024-Q3**
#### The Conduct Committee received **0 incident report(s) in 2024-Q2**
#### The Conduct Committee received **0 incident report(s) in 2024-Q1**

## Responsibilities of the Conduct Committee
- Review and Investigate Complaints: The committee will be responsible for reviewing and, if necessary, investigating any complaints or reports of violations of the Code of Conduct.
- Recommend Actions: Based on investigations, the committee will suggest appropriate responses to violations.
- Promote Education and Awareness: The committee will raise awareness about the Code of Conduct and provide educational opportunities to prevent violations and foster inclusivity.

## Composition of the Conduct Committee
The Conduct Committee will consist of a diverse group of individuals who are deeply committed to nurturing a healthy and inclusive community. The committee members will be selected based on their experience, expertise, and commitment to upholding the values outlined in the Code of Conduct. 

Currently the Conduct Commiitte members are:
- Emily Jiang (IBM)
- Irina Artemeva (AMD)
- Matthew Khouzam (Ericsson)
- Jonas Helming (EclipseSource)
- Maria Teresa Delgado (Eclipse Foundation) - Chair of the Conduct Committee

## Reporting Violations
If you encounter or witness a violation of the [Eclipse Foundation Code of Conduct](https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php), we strongly encourage you to report it to **conduct@eclipse-foundation.org**. Your reports will be handled with the utmost confidentiality and care by the committee.

## Getting Involved
While the initial committee members will be appointed, we encourage anyone who is interested in contributing to the committee's work to express their interest. As we move forward, there may be opportunities for additional community members to join the committee or participate in related initiatives.

##

We believe that the establishment of the Conduct Committee is a significant step towards creating a more inclusive and respectful Eclipse Foundation community, aligned with our core values of respect, inclusivity, and professionalism. We thank you for your continued support and commitment to these shared values.

If you have any questions or would like more information about the Conduct Committee or the Code of Conduct, please feel free to reach out to us.

